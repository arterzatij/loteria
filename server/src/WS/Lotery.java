package WS;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import user.Player;

import card.Card;

import board.Board;
import game.Game;



public class Lotery {
	private Game game;
	
	public Lotery(){
		game = new Game();
	}
	
	public static void main(String[] args) {
		Lotery lotery = new Lotery();
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader (isr);
		try {
			System.out.println("Player 1: ");
			String playername = br.readLine();
			lotery.addPlayer(playername);
			System.out.println("Elige board: ");
			for(Integer id : lotery.getBoards()){
				System.out.println(id);
			}
			int board =Integer.parseInt(br.readLine());
			lotery.assignBoard(board, playername);
			/*System.out.println("Player 2: ");
			player = br.readLine();
			lotery.addPlayer(player);
			System.out.println("Elige board: ");
			for(Integer id : lotery.getBoards()){
				System.out.println(id);
			}
			board =Integer.parseInt(br.readLine());
			lotery.assignBoard(board, player);*/
			Player player = lotery.getPlayer(playername);
			System.out.println(lotery.getCurrentCard());
			System.out.println(player.getBoard());
			lotery.validateCard(34, playername);
			System.out.println(player.getBoard());
			System.out.println(lotery.nextCard());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void addPlayer(String name) {
		game.addPlayer(name);
	}

	public List<Integer> getBoards() {
		List<Integer> boards = new ArrayList<Integer>();
		for(Board board : game.getBoards()){
			boards.add(board.getId());
		}
		return boards;
	}

	public void assignBoard(int id, String name) {
		game.assignBoard(id, name);
	}

	public void validateCard(int id, String name) {
		game.validateCard(id, name);
	}

	public Card nextCard() {
		return game.nextCard();
	}

	public Card getCurrentCard() {
		return game.getCurrentCard();
	}
	
	public Player getPlayer(String player){
		return game.get(player);
	}
}