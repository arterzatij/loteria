package board;

import java.lang.reflect.Field;
import java.util.ArrayList;
import card.Card;
import card.CardType;
import card.Cards;

public class Board extends ArrayList<Card> {

	/**
	 * 
	 */
	
	private int id;
	
	private static final long serialVersionUID = 5903516877220254973L;

	public Board(BoardType boardType) {
		
		id = boardType.id();
		
		for (int i = 0; i < boardType.cards().length; i++) {

			int cardId = boardType.cards()[i];
			

			Field field = null;
			try {
				field = Cards.class.getDeclaredField("_" + cardId);
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			}

			CardType cardType = field.getAnnotation(CardType.class);

			this.add(new Card(cardType));

		}

	}

	public Card get(int m, int n) {
		return get((n * 4) + m);
	}
	
	public int getId(){
		return id;
	}

}
