package board;

public class Boards {
	@BoardType(cards={46,34,26,18,10,2,41,44,14,36,28,20,12,4,43,35}, id = 1)
	  private Board _1;

	  @BoardType(cards={27,19,11,3,33,25,17,9,1,42,38,30,22,6,45,48}, id = 2)
	  private Board _2;

	  @BoardType(cards={40,32,24,16,8,47,39,31,23,15,7,37,29,21,13,5}, id =3)
	  private Board _3;

	  @BoardType(cards={49,28,54,17,15,12,26,53,50,3,46,52,22,40,51,37}, id = 4)
	  private Board _4;

	  @BoardType(cards={32,33,2,31,41,42,47,4,39,51,34,35,21,3,43,44}, id = 5)
	  private Board _5;

	  @BoardType(cards={36,48,39,30,45,51,34,35,40,10,38,39,31,43,44,3}, id = 6)
	  private Board _6;

	  @BoardType(cards={21,22,23,24,30,31,40,33,39,19,41,42,48,49,50,51}, id = 7)
	  private Board _7;

	  @BoardType(cards={1,2,3,4,10,11,12,13,19,20,21,22,28,29,30,31}, id = 8)
	  private Board _8;

	  @BoardType(cards={9,21,50,7,14,4,20,2,38,31,4,5,23,16,43,53}, id = 9)
	  private Board _9;

	  @BoardType(cards={31,43,14,1,8,30,18,25,32,41,47,53,10,15,51,13}, id = 10)
	  private Board _10;

	  @BoardType(cards={18,19,13,23,48,37,3,26,21,28,30,29,34,38,45,44}, id = 11)
	  private Board _11;

	  @BoardType(cards={27,24,8,35,36,40,42,53,31,50,51,19,16,32,46,49}, id = 12)
	  private Board _12;

	  @BoardType(cards={33,50,24,36,34,41,13,20,15,26,54,42,40,6,11,21}, id = 13)
	  private Board _13;

	  @BoardType(cards={11,25,46,12,2,43,28,24,31,49,42,36,47,51,44,50}, id = 14)
	  private Board _14;

	  @BoardType(cards={20,29,3,10,18,22,8,33,39,45,54,32,7,9,23,25}, id = 15)
	  private Board _15;

	  @BoardType(cards={52,34,11,23,51,14,41,38,50,54,47,48,49,37,6,44}, id = 16)
	  private Board _16;
}
