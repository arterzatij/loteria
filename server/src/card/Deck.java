package card;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.Random;
import java.util.Stack;

public class Deck extends Stack<Card>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8041423571957910662L;

	public Deck(){
		Field fields[] = Cards.class.getDeclaredFields();
		for(int i = 0;i<fields.length;i++){
			Field field = fields[i];
			CardType cardType = field.getAnnotation(CardType.class);
			this.push(new Card(cardType));
		}
	}
	
	public void shuffle(){
		Collections.shuffle(this, new Random(this.size()));
	}
	
	public Boolean hasCard(){
		return !this.empty();
	}
	
	public Card nextCard(){
		return this.pop();
	}
	
}
