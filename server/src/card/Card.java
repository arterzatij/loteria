package card;

public class Card {
	private int id;
	private String name;
	private boolean beaned;

	
	public Card(CardType cardType){
		this.id = cardType.id();
		this.name = cardType.name();
	}
	
	public Card(int id){
		this.id = id;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Card)) {
			return false;
		}
		return id == ((Card) obj).getId();
	}

	@Override
	public int hashCode() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isBeaned() {
		return beaned;
	}

	public void setBeaned(boolean beaned) {
		this.beaned = beaned;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Card [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", beaned=");
		builder.append(beaned);
		builder.append("]");
		return builder.toString();
	}

	
	
	
}
